#!/usr/bin/python3

import ccxt

def main():
    print("  List of CCXT supported exchanges:")
    print(ccxt.exchanges)
    print("  Exiting.")


if __name__ == "__main__":
    main()
