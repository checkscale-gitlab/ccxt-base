FROM alpine:3.13

RUN apk add --no-cache \
    python3-dev \
    musl-dev \
    g++ \
    libffi-dev \
    libressl-dev \
    py3-pip \
    tzdata \
    cargo \
  && pip3 install --no-cache-dir --upgrade \
    pip \
    setuptools \
  && pip3 install --no-cache-dir \
    wheel \
  && pip3 install --no-cache-dir \
    ccxt \
  && apk del \
    python3-dev \
    musl-dev \
    g++ \
    libffi-dev \
    libressl-dev

COPY docker-root /
RUN chmod +x /*.py

CMD ["/entrypoint.py"]
